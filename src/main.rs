// Copyright 2019 Matt Kraai

// This file is part of recreation-gov.

// recreation-gov is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by the
// Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.

// recreation-gov is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
// License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with recreation-gov.  If not, see <https://www.gnu.org/licenses/>.

use chrono::NaiveDate;
use reqwest::Client;
use serde_derive::Deserialize;
use std::collections::HashMap;
use structopt::StructOpt;

#[derive(Deserialize)]
struct SearchResponse {
    entity_id: String,
}

#[derive(Deserialize)]
struct SearchCampsitesResponse {
    campsites: Vec<Campsite>,
}

#[derive(Deserialize)]
struct Campsite {
    name: String,
    campsite_id: String,
}

#[derive(Deserialize)]
struct LoginResponse {
    access_token: String,
    account: Account,
}

#[derive(Deserialize)]
struct Account {
    account_id: String,
}

#[derive(StructOpt)]
enum Opt {
    /// Prints the campsites at a campground
    #[structopt(name = "campsites")]
    Campsites {
        campground: String,
    },
    /// Reserves a campsite
    #[structopt(name = "reserve")]
    Reserve {
        username: String,
        password: String,
        campsite_id: String,
        check_in: NaiveDate,
        check_out: NaiveDate,
    },
}

const BASE_URL: &'static str = "https://www.recreation.gov/api";

fn main() {
    let opt = Opt::from_args();
    let client = Client::new();
    match opt {
        Opt::Campsites { campground } => campsites(client, campground),
        Opt::Reserve { username, password, campsite_id, check_in, check_out } => reserve(client, username, password, campsite_id, check_in, check_out),
    }
}

fn campsites(client: Client, campground: String) {
    let campground_id = client.get(&format!("{}/search", BASE_URL)).query(&[("q", campground.as_str())]).send().unwrap().json::<SearchResponse>().unwrap().entity_id;
    for campsite in client.get(&format!("{}/search/campsites", BASE_URL)).query(&[("fq", format!("asset_id:{}", campground_id).as_str()), ("size", "1000")]).send().unwrap().json::<SearchCampsitesResponse>().unwrap().campsites {
        println!("{}: {}", campsite.name.trim(), campsite.campsite_id);
    }
}

fn reserve(client: Client, username: String, password: String, campsite_id: String, check_in: NaiveDate, check_out: NaiveDate) {
    let mut body = HashMap::new();
    body.insert("username", username);
    body.insert("password", password);
    let response = client.post(&format!("{}/accounts/login", BASE_URL)).json(&body).send().unwrap().json::<LoginResponse>().unwrap();
    body.clear();
    body.insert("account_id", response.account.account_id);
    body.insert("campsite_id", campsite_id);
    body.insert("check_in", format!("{}T00:00:00Z", check_in));
    body.insert("check_out", format!("{}T00:00:00Z", check_out));
    let mut response = client.post(&format!("{}/camps/reservations", BASE_URL)).bearer_auth(response.access_token).json(&body).send().unwrap();
    println!("{} {}", response.status(), response.text().unwrap());
}
